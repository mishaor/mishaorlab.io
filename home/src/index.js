import Index from './Index.svelte';

import './global.css';

const index = new Index({
    target: document.body
});

export default index;

const { resolve } = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'bundle.[hash].js',
        path: resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            { test: /\.svelte$/, use: 'svelte-loader' },
            { test: /\.css$/, use: [
                'style-loader',
                'css-loader'
            ]}
        ]
    },
    plugins: [
        new (require('html-webpack-plugin'))({
            template: './src/index.html'
        }),
        new (require('clean-webpack-plugin').CleanWebpackPlugin)(),
    ]
}
